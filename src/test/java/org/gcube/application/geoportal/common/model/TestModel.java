package org.gcube.application.geoportal.common.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.gcube.application.geoportal.common.model.legacy.AccessPolicy;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;



public class TestModel {

	
	public static Concessione prepareEmptyConcessione() {
		Concessione concessione=new Concessione();

		// Generic fields
			
		// Concessione fields

		concessione.setNome("MONGO Italia, forse, ma su ");
		concessione.setIntroduzione("This is my MONGO project");
		concessione.setDescrizioneContenuto("It contains this and that");

		concessione.setAuthors(Arrays.asList(new String[] {"Some one","Some, oneelse"}));

		concessione.setContributore("Contrib 1");
		concessione.setTitolari(Arrays.asList(new String[] {"Some one","Some, oneelse"}));
		concessione.setResponsabile("Someone");
		concessione.setEditore("Editore");

		concessione.setFontiFinanziamento(Arrays.asList(new String[] {"Big pharma","Pentagon"}));


		concessione.setSoggetto(Arrays.asList(new String[] {"Research Excavation","Archeology"}));


		concessione.setDataInizioProgetto(LocalDateTime.now());
		concessione.setDataFineProgetto(LocalDateTime.now());

		concessione.setLicenzaID("CC-BY");
		
		concessione.setTitolareLicenza(Arrays.asList(new String[] {"Qualcun altro"}));
		concessione.setTitolareCopyright(Arrays.asList(new String[] {"Chiedilo in giro"}));

		concessione.setParoleChiaveLibere(Arrays.asList(new String[] {"Robba","Stuff"}));
		concessione.setParoleChiaveICCD(Arrays.asList(new String[] {"vattelapesca","somthing something"}));


//		concessione.setCentroidLat(43.0); //N-S
//		concessione.setCentroidLong(9.0); //E-W
//
		return concessione;
	}
	
	public static final Concessione setIds(Concessione c) {
//		c.setMongo_id(rnd());
		c.getRelazioneScavo().setMongo_id(rnd());
		c.getPosizionamentoScavo().setMongo_id(rnd());
		c.getPianteFineScavo().forEach((LayerConcessione l)->{l.setMongo_id(rnd());});
		c.getImmaginiRappresentative().forEach((UploadedImage i)->{i.setMongo_id(rnd());});
		return c;
	}
	
	public  static final String rnd() { 
		return new ObjectId().toHexString();
	}
	public static Concessione prepareConcessione() {
		return prepareConcessione(4,2);
	}
	
	public static Concessione prepareConcessione(int pianteCount	,int imgsCount) {
		
		Concessione concessione=prepareEmptyConcessione();

		
		
		// Attachments

		// Relazione scavo
		RelazioneScavo relScavo=new RelazioneScavo();

		relScavo.setAbstractEng("simple abstract section");
		relScavo.setAbstractIta("semplice sezione abstract");
		relScavo.setResponsabili(concessione.getAuthors());

		concessione.setRelazioneScavo(relScavo);
		//Immagini rappresentative
		ArrayList<UploadedImage> imgs=new ArrayList<>();
		for(int i=0;i<imgsCount;i++) {
			UploadedImage img=new UploadedImage();
			img.setTitolo("My image number "+i);
			img.setDidascalia("You can see my image number "+i);
			img.setFormat("TIFF");
			img.setCreationTime(LocalDateTime.now());
			img.setResponsabili(concessione.getAuthors());
			imgs.add(img);
		}
		concessione.setImmaginiRappresentative(imgs);
		//Posizionamento
		LayerConcessione posizionamento=new LayerConcessione();
		posizionamento.setValutazioneQualita("Secondo me si");
		posizionamento.setMetodoRaccoltaDati("Fattobbene");
		posizionamento.setScalaAcquisizione("1:10000");
		posizionamento.setAuthors(concessione.getAuthors());	
		concessione.setPosizionamentoScavo(posizionamento);

		// Piante fine scavo
		ArrayList<LayerConcessione> piante=new ArrayList<LayerConcessione>();
		for(int i=0;i<pianteCount;i++) {
			LayerConcessione pianta=new LayerConcessione();
			pianta.setValutazioneQualita("Secondo me si");
			pianta.setMetodoRaccoltaDati("Fattobbene");
			pianta.setScalaAcquisizione("1:10000");
			pianta.setAuthors(concessione.getAuthors());	
			pianta.setPolicy(AccessPolicy.RESTRICTED);
			piante.add(pianta);
		}
		concessione.setPianteFineScavo(piante);
		
		return concessione;
	}
	
}
