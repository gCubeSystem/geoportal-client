package org.gcube.application.geoportal.common.model;

import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

public class Serializations {

    @Test
    public void readOne(){}

    @Test
    public void readMulti() throws IOException {

        Iterator it=Serialization.readCollection(
                new FileInputStream(new File("src/test/resources/concessioni/ConcessioniList.json")),
                        Concessione.class);
        AtomicLong l=new AtomicLong(0);
        it.forEachRemaining(element->{l.incrementAndGet();});


        Assert.assertTrue(l.get()==4);
    }

}
