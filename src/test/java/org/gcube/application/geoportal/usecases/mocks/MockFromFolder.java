package org.gcube.application.geoportal.usecases.mocks;

import com.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;
import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.utils.FileSets;
import org.gcube.application.geoportal.clients.StatelessClientTests;
import org.gcube.application.geoportal.clients.TokenSetter;
import org.gcube.application.geoportal.common.model.TestModel;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.contentmanagement.blobstorage.service.IClient;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.mongoConcessioni;
import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;
import static org.junit.Assert.assertTrue;

@Slf4j
public class MockFromFolder {

    public static void main(String[] args) throws Exception {
        //PARAMS
        String context="/gcube/devsec/devVRE";

        //Concessioni 04-03
        //String packageBaseDir="/Users/fabioisti/Documents/Concessioni 04-03/";
        //String csvDescriptor="src/test/resources/concessioni/concessioni04-03.csv";

        //DATASET_GNA_01
//        String packageBaseDir="/Users/fabioisti/Documents/DATASET_GNA_01";
//        String csvDescriptor="src/test/resources/concessioni/DATASET_GNA_01.csv";

        //DATASET_GNA_02
        //String packageBaseDir="/Users/fabioisti/Documents/DATASET_GNA_02";
        //String csvDescriptor="src/test/resources/concessioni/DATASET_GNA_02.csv";


        // invio_08_02
//        String packageBaseDir="/Users/fabioisti/Documents/invio_08_05";
//        String csvDescriptor="src/test/resources/concessioni/invio_08_05.csv";

        // concessioni 23_04
        String packageBaseDir="/Users/fabioisti/Documents/Concessioni_23_04";
        String csvDescriptor="src/test/resources/concessioni/concessioni_23_04.csv";




        TokenSetter.set(context);

        MongoConcessioni client=mongoConcessioni().build();
        StorageUtils storage=new StorageUtils();


        long publishedCount=0l;
        long successcount=0l;
        long entrycount=0l;

        //Parse CSV descriptor
        File baseDir=new File(packageBaseDir);
        ArrayList<Concessione> pushed=new ArrayList<>();

        CSVReader reader = new CSVReader(new FileReader(csvDescriptor));
        String [] nextLine;
        //reads one line at a time
        while ((nextLine = reader.readNext()) != null)
        {
            entrycount++;
                //Create new
                String projectName = nextLine[0];
                String positionPath = nextLine[1];
                String piantePath = nextLine[2];


            try {
                //NB raggruppa per file
                Map.Entry<String,List<File>> posSets = clusterizeFiles(positionPath, baseDir).entrySet().stream().findFirst().get();
                Map<String, List<File>> pianteSets = clusterizeFiles(piantePath, baseDir);

                // Sometimes they are the same
                if(positionPath.equals(piantePath))
                    pianteSets.remove(posSets.getKey());

                log.debug("Entry {} pos Size {} piante {} ",projectName,posSets.getValue().size(),pianteSets.size());

                Concessione c = createMock(projectName, pianteSets, posSets.getValue(), client, storage);

                publishedCount++;
                if (c.getReport().getStatus().equals(ValidationReport.ValidationStatus.PASSED))
                    successcount++;
                pushed.add(c);
            }catch(Throwable t){
                System.err.println("Problematic entry "+projectName);
                t.printStackTrace(System.err);
            }

        }

        System.out.println("Done "+publishedCount+" [SUCCESS : "+successcount+"] \t OUT OF :"+entrycount+" entries");

        pushed.forEach(c -> {
            try{
                System.out.println(c.getNome()+"\t"+c.getMongo_id()+"\t"+c.getReport().getStatus());
            }catch(Throwable t){
                System.out.println(c.getNome()+"\t"+c.getMongo_id()+"\t PROBLEMATIC, NO REPORT");
            }
        });
    }



    private static Map<String,List<File>> clusterizeFiles(String basePath,File packageFolder) throws IOException {
        log.debug("Clusterizing "+basePath);

        HashMap<String,List<File>> toReturn = new HashMap<>();
        File baseDir=new File(packageFolder,basePath);
        for(File shp:baseDir.listFiles((dir,name)->{return name.endsWith(".shp");})) {
            String basename=shp.getName().substring(0,shp.getName().lastIndexOf("."));
            List<File> fileset=new ArrayList<>();
            for (File shpSet : baseDir.listFiles((dir, name) -> {return name.startsWith(basename);}))
                fileset.add(shpSet);
            log.debug("SHP {} Set size {} ",basename,fileset.size());
            toReturn.put(basename,fileset);
        }
        return toReturn;
    }

    private static Concessione createMock(String baseName,Map<String,List<File>> piante, List<File> pos,
                                          MongoConcessioni client, StorageUtils storage) throws Exception {

        Concessione c=TestModel.prepareConcessione(piante.size(), 2);
        c.setNome("Mock for "+baseName);
        c= client.createNew(c);
        String mongoId=c.getMongo_id();

        // TEST DATA, DO NOT CARE
        client.registerFileSet(mongoId, FileSets.prepareRequest(storage,
                Concessione.Paths.RELAZIONE,new File ("src/test/resources/concessioni/relazione.pdf")));

        client.registerFileSet(mongoId, FileSets.prepareRequest(storage,
                Concessione.Paths.imgByIndex(0),new File("src/test/resources/concessioni/immagine.png")));

        // POSIZIONAMENTO

        client.registerFileSet(mongoId, FileSets.prepareRequest(storage,
                Concessione.Paths.POSIZIONAMENTO,pos.toArray(new File[pos.size()])));

        Map.Entry<String,List<File>>[] entries= piante.entrySet().toArray(new Map.Entry[0]);
        for( int i= 0; i< piante.size();i++)
            client.registerFileSet(mongoId, FileSets.prepareRequest(storage,
                    Concessione.Paths.piantaByIndex(i),entries[i].getValue().toArray(new File[0])));

        c=client.publish(mongoId);

        System.out.println("@@@ Concessione "+c.getNome()+"\t STATUS : "+ c.getReport().getStatus());

        return c;
    }

}
