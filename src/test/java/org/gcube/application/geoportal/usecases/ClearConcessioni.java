package org.gcube.application.geoportal.usecases;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.clients.TokenSetter;
import org.gcube.application.geoportal.common.model.legacy.Concessione;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;

public class ClearConcessioni {

    public static void main(String[] args) throws Exception {
        TokenSetter.set("/gcube/devsec/devVRE");

        ConcessioniManagerI manager=statefulMongoConcessioni().build();


        ArrayList<String> toSkipIds=new ArrayList<>();
//        toSkipIds.add("6102c8dd02ad3d05b5f81df4");
//        toSkipIds.add("610415af02ad3d05b5f81ee3");

        AtomicLong count=new AtomicLong(0);
        AtomicLong nullCount=new AtomicLong(0);
        AtomicLong errCount=new AtomicLong(0);


        Iterator<Concessione> it=null;
//        it=manager.getList();
        it=manager.search("{\"centroidLat\" : 0}");

        it.forEachRemaining((Concessione c)->{
            try{
                String currentId=c.getMongo_id();
                if(currentId==null) {
                    System.out.println("ID IS NULL " + c);
                    nullCount.incrementAndGet();
                }
                else
                    if(toSkipIds.contains(currentId))
                        System.out.println("Skipping "+currentId);
                    else {
                        System.out.println("Deleting " + c.getMongo_id());
                        manager.deleteById(c.getMongo_id(),true);
                    }
            }catch(Throwable throwable){
                System.err.println(throwable);
                errCount.incrementAndGet();
            }finally {
                count.incrementAndGet();
            }
        });

        System.out.println("Done "+count.get()+" [null : "+nullCount.get()+", err : "+errCount.get()+"]");

    }
}
