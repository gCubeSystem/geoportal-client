package org.gcube.application.geoportal.usecases;

import lombok.extern.slf4j.Slf4j;
import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.clients.TokenSetter;
import org.gcube.application.geoportal.common.model.legacy.*;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;

@Slf4j
/*
Pushes concessioni JSON from folder to geoportal-service
 */
public class Export {


    public static void main(String[] args) {

        File dir= new File("/Users/fabioisti/git/geoportal-client/import1628178107083");
//        String targetContext="/pred4s/preprod/preVRE";
        String targetContext="/gcube/devsec/devVRE";



        ArrayList<Concessione> found=new ArrayList<>();
        for(File elementFolder:dir.listFiles())
            for(File jsonFile:elementFolder.listFiles((dir1, name) -> {return name.endsWith(".json");}))
                try {
                    log.info("Reading "+jsonFile.getAbsolutePath());
                    String json= Files.readFileAsString(jsonFile.getAbsolutePath(), Charset.defaultCharset());
                    found.add(Serialization.read(json,Concessione.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }

        System.out.println("Loaded "+found.size()+" elements from "+dir.getAbsolutePath());

            TokenSetter.set(targetContext);
            ConcessioniManagerI targetManager = statefulMongoConcessioni().build();
            StorageUtils storage = new StorageUtils();

            AtomicLong count = new AtomicLong(0);
            AtomicLong warnCount = new AtomicLong(0);
            AtomicLong errCount = new AtomicLong(0);

            for (Concessione c : found) {
                try {
                    log.info("Using {} {}",c.getNome(),c.getMongo_id());
                    Concessione result = push(c, targetManager, new File(dir.getAbsolutePath(),
                            c.getId() + ""), storage);
                    if (!result.getReport().getStatus().equals(ValidationReport.ValidationStatus.PASSED))
                        warnCount.incrementAndGet();
                } catch (Throwable throwable) {
                    System.err.println(throwable);
                    errCount.incrementAndGet();
                } finally {
                    count.incrementAndGet();
                }
            }

        System.out.println("Done "+count.get()+" [warn : "+warnCount.get()+", err : "+errCount.get()+"]");
    }

    public static Concessione push(Concessione c, ConcessioniManagerI manager, File dir, StorageUtils storage) throws Exception {
        // remove GIS references
        LayerConcessione posizionamento= c.getPosizionamentoScavo();
        c.setPosizionamentoScavo(null);

        List<LayerConcessione> piante=c.getPianteFineScavo();
        c.setPianteFineScavo(new ArrayList<LayerConcessione>());

        List<UploadedImage> imgs=c.getImmaginiRappresentative();
        c.setImmaginiRappresentative(new ArrayList<UploadedImage>());

        List<OtherContent> other=c.getGenericContent();
        c.setGenericContent(new ArrayList<OtherContent>());

        RelazioneScavo rel=c.getRelazioneScavo();
        c.setRelazioneScavo(null);


        // remove source folder id
        c.setFolderId(null);
        c.setMongo_id(null);

        // PUSH PROJECT
        manager.createNew(c);


        // UPLOAD WOrKSPaCE content
        // Relazione
        removeContent(rel);
        manager.setRelazioneScavo(rel, fromPath(dir.toPath().toAbsolutePath()+File.separator+"rel",storage)[0]);

        // Posizionamento
        removeContent(posizionamento);
        manager.setPosizionamento(posizionamento,
                fromPath(dir.toPath().toAbsolutePath()+File.separator+"pos", storage));


        //Piante
        for(int i=0; i<piante.size();i++) {
            LayerConcessione l=piante.get(i);
            removeContent(l);
            manager.addPiantaFineScavo(l,
                    fromPath(dir.toPath().toAbsolutePath()+File.separator+"pianta_"+i, storage));
        }

        //Immagini Rappresentative
        for(int i=0; i<imgs.size();i++) {
            UploadedImage img=imgs.get(i);
            removeContent(img);
            manager.addImmagineRappresentativa(img,
                    fromPath(dir.toPath().toAbsolutePath()+File.separator+"imgs_"+i, storage)[0]);
        }

        //Other content
//        for(int i=0; i<other.size();i++) {
//            OtherContent otherContent=other.get(i);
//            removeContent(otherContent);
//            manager.a(otherContent,
//                    fromPath(dir.toPath().toAbsolutePath()+File.separator+"imgs_"+i));
//        }


        // publish
        return manager.publish();
    }

    private static TempFile[] fromPath(String path, StorageUtils storage) throws FileNotFoundException {
        Path baseDir= Paths.get(path);
        ArrayList<TempFile> toReturn=new ArrayList<>();
        for(File f : baseDir.toFile().listFiles()){
            toReturn.add(storage.putOntoStorage(new FileInputStream(f),f.getName()));
        }
        log.info("Loaded {} files from {} ",toReturn.size(),path);
        return toReturn.toArray(new TempFile[toReturn.size()]);
    }


    private static void removeContent(AssociatedContent c){ c.setActualContent(new ArrayList<>());}

}
