package org.gcube.application.geoportal.usecases;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.clients.TokenSetter;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;

public class UnpublishSingle {

    public static void main(String[] args) throws Exception {
        TokenSetter.set("/gcube/devsec/devVRE");

        ConcessioniManagerI manager=statefulMongoConcessioni().build();

        manager.publish("610415af02ad3d05b5f81ee3");
    }
}
