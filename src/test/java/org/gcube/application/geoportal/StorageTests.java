package org.gcube.application.geoportal;

import org.gcube.application.geoportal.clients.TokenSetter;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class StorageTests {

    public static void main(String[] args) throws FileNotFoundException {
        FileInputStream fis =new FileInputStream(Files.getFileFromResources("concessioni/relazione.pdf"));
        String filename= "relazione.pdf";

        String sourceContext="/d4science.research-infrastructures.eu";
        String targetContext="/pred4s/preprod/preVRE";

        TokenSetter.set(sourceContext);

        StorageHubClient sgClient=new StorageHubClient();

        TokenSetter.set(targetContext);

        TempFile file= new StorageUtils().putOntoStorage(fis,filename);


        System.out.println(new StorageUtils().getUrlById(file.getId()));

    }

}
