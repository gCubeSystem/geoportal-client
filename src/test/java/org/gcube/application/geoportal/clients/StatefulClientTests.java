package org.gcube.application.geoportal.clients;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.TestModel;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport.ValidationStatus;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.junit.Test;


public class StatefulClientTests extends BasicVreTests{

	
	
	private Concessione publishNew() throws Exception {
		ConcessioniManagerI manager=statefulMongoConcessioni().build();
		StorageUtils storage=new StorageUtils();
		
		
		Concessione toRegister= TestModel.prepareEmptyConcessione();
		toRegister.setNome("Mock module");
		manager.createNew(toRegister);
		
		UploadedImage toRegisterImg=TestModel.prepareConcessione().getImmaginiRappresentative().get(0);
		
		// TEMP Files are hosted in INFRASTRUCTURE's VOLATILE AREA
		TempFile toUpload=storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/immagine.png")), "immagine.png");
		manager.addImmagineRappresentativa(toRegisterImg, toUpload);
		
		//Alternative Method
		InputStreamDescriptor isDesc=new InputStreamDescriptor(new FileInputStream(Files.getFileFromResources("concessioni/immagine.png")), "immagine.png");		
		manager.addImmagineRappresentativa(toRegisterImg, isDesc);
		
		
		
		//Relazione
		manager.setRelazioneScavo(TestModel.prepareConcessione().getRelazioneScavo(),
				storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/relazione.pdf")), "relazione_it.pdf"),
				storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/relazione.pdf")), "relazione_en.pdf"));
		
		// Posizionamento scavo
		manager.setPosizionamento(TestModel.prepareConcessione().getPosizionamentoScavo(),
				storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/pos.shp")), "pos.shp"));
		
		// Piante 
		manager.addPiantaFineScavo(TestModel.prepareConcessione().getPianteFineScavo().get(0),
				storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/pianta.shp")), "pianta.shp"),
				storage.putOntoStorage(new FileInputStream(Files.getFileFromResources("concessioni/pianta.shx")), "pianta.shx"));
		
		return manager.publish();
	}
	

	
	@Test
	public void testRegisterNew() throws RemoteBackendException, FileNotFoundException, Exception {
		Concessione published=publishNew();

		
		// VARIOUS CHECKS
		assertNotNull(published.getReport());
		assertEquals(published.getReport().getStatus(),ValidationStatus.PASSED);
		
		assertEquals(published.getImmaginiRappresentative().size(),2);
		assertEquals(published.getPianteFineScavo().size(),1);
		assertNotNull(published.getPosizionamentoScavo().getWmsLink());
		for(LayerConcessione l : published.getPianteFineScavo())
			assertNotNull(l.getWmsLink());
		assertNotNull(published.getCentroidLat());
		assertNotNull(published.getCentroidLong());
		
		System.out.println(Serialization.write(published));
		System.out.println(Serialization.write(published.getReport()));
	}
	
	@Test
	public void delete() throws Exception {
		ConcessioniManagerI manager=statefulMongoConcessioni().build();
		StorageUtils storage=new StorageUtils();
		
		manager.createNew(TestModel.prepareEmptyConcessione());

		manager.delete();
	}
	
	@Test
	public void replace() {
		
	}
	
	@Test
	public void getById() {
		
	}
	
	@Test
	public void list() {
		
	}
}
