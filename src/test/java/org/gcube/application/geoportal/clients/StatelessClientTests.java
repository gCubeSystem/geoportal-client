package org.gcube.application.geoportal.clients;

import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.mongoConcessioni;
import static org.gcube.application.geoportal.client.GeoportalAbstractPlugin.statefulMongoConcessioni;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.utils.Queries;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.TestModel;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.Concessione.Paths;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport.ValidationStatus;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.junit.Test;

public class StatelessClientTests extends BasicVreTests{

	
	private MongoConcessioni client=mongoConcessioni().build();
	
	@Test
	public void testCreateNew() throws Exception {
		Concessione c= client.createNew(TestModel.prepareEmptyConcessione());
		assertNotNull(c);
		assertNotNull(c.getMongo_id());
	}
	
	@Test
	public void testDeleteById() throws Exception {
		Concessione c= client.createNew(TestModel.prepareEmptyConcessione());
		client.deleteById(c.getMongo_id());
	}
	
	@Test
	public void testList() throws Exception {
		final AtomicLong counter=new AtomicLong();
		long before=System.currentTimeMillis();
		client.getList().forEachRemaining((Concessione c)-> {counter.addAndGet(1);});
		System.out.println("Loaded "+counter+" in "+(System.currentTimeMillis()-before)+" ms");
	}
	
	@Test
	public void testReplace() throws Exception {
		Concessione c= client.createNew(TestModel.prepareEmptyConcessione());
		
		String title="My new shiny Title";
		c.setNome(title);
		Concessione c1=client.replace(c);
		assertEquals(title, c1.getNome());
	}
	
	@Test
	public void testUploadFileSet() throws Exception {
		Concessione c= client.createNew(TestModel.prepareConcessione());
		assertNotNull(c.getRelazioneScavo());
		AddSectionToConcessioneRequest request=
				new AddSectionToConcessioneRequest(Paths.RELAZIONE, 
						Collections.singletonList(new StorageUtils().putOntoStorage(
								new FileInputStream(Files.getFileFromResources("concessioni/relazione.pdf")), "relazione.pdf")));
		
		c= client.registerFileSet(c.getMongo_id(), request);
		
		assertNotNull(c.getRelazioneScavo().getActualContent());
		assertNotNull(c.getRelazioneScavo().getActualContent().get(0));
	}
	
	@Test
	public void testPublsh() throws Exception {
		publish(true);
	}


	public Concessione publish(Boolean verify) throws Exception {
		return publish(verify,client);
	}



	public static Concessione publish(Boolean verify,MongoConcessioni client) throws Exception {
		Concessione c= client.createNew(TestModel.prepareConcessione());
		
		String mongoId=c.getMongo_id();
		
		AddSectionToConcessioneRequest request=
				new AddSectionToConcessioneRequest(Paths.RELAZIONE, 
						Collections.singletonList(new StorageUtils().putOntoStorage(
								new FileInputStream(Files.getFileFromResources("concessioni/relazione.pdf")), "relazione.pdf")));
		
		client.registerFileSet(mongoId, request);
		
		request=
				new AddSectionToConcessioneRequest(Paths.imgByIndex(0), 
						Collections.singletonList(new StorageUtils().putOntoStorage(
								new FileInputStream(Files.getFileFromResources("concessioni/immagine.png")), "immagine.png")));
		
		client.registerFileSet(mongoId, request);
		
		
		request=
				new AddSectionToConcessioneRequest(Paths.POSIZIONAMENTO, 
						Collections.singletonList(new StorageUtils().putOntoStorage(
								new FileInputStream(Files.getFileFromResources("concessioni/pos.shp")), "pos.shp")));
		
		client.registerFileSet(mongoId, request);
		
		request=
				new AddSectionToConcessioneRequest(Paths.piantaByIndex(0), 
						Collections.singletonList(new StorageUtils().putOntoStorage(
								new FileInputStream(Files.getFileFromResources("concessioni/pianta.shp")), "pianta.shp")));
		
		client.registerFileSet(mongoId, request);
		
		
		c=client.publish(mongoId);

		if(verify)
			assertTrue(c.getReport().getStatus().equals(ValidationStatus.PASSED));

		return c;
	}


	@Test
	public void getConfiguration() throws Exception {
		System.out.println(client.getCurrentConfiguration());
	}

	@Test
	public void searches() throws Exception {
		for(File filterFile:new File("src/test/resources/concessioni/filters").listFiles()) {
			String query=Files.readFileAsString(filterFile.getAbsolutePath(), Charset.defaultCharset());
			System.out.println("Count for "+filterFile.getName()+"\t"+ count(client.search(query)));
		}
	}


	@Test
	public void query() throws Exception {
		// No Transformation
		System.out.print("First Registered \t");
		Iterator<Concessione> queriedDocuments=client.query(
				Queries.readPath("src/test/resources/concessioni/queries/firstRegistered.json"));
		// Expected one result
		assertTrue(count(queriedDocuments)==1);



		System.out.print("Last Registered \t");
		// Expected one result
		queriedDocuments=client.query(
				Queries.readPath("src/test/resources/concessioni/queries/lastRegistered.json"));
		assertTrue(count(queriedDocuments)==1);

		queriedDocuments.forEachRemaining((Concessione c)->{System.out.println(c.getNome());});


		// Transformations
		System.out.println(
				client.queryForJSON(
						Queries.readPath("src/test/resources/concessioni/queries/lastNameRegisteredByFabio.json")));

		System.out.println(
				client.queryForJSON(
						Queries.readPath("src/test/resources/concessioni/queries/publicationWarningMessages.json")));






//			String query=Files.readFileAsString(filterFile.getAbsolutePath(), Charset.defaultCharset());
//			System.out.println("Count for "+filterFile.getName()+"\t"+ count(client.search(query)));
//		}
	}

	@Test
	public void testCleanFileSet() throws Exception {

		Concessione c=publish(false);
		client.unPublish(c.getMongo_id());

		//Precheck to be sure
		assertFalse(c.getPosizionamentoScavo().getActualContent().isEmpty());
		assertFalse(c.getPianteFineScavo().get(0).getActualContent().isEmpty());

		//Clear pos
		c=client.cleanFileSet(c.getMongo_id(),Paths.POSIZIONAMENTO);
		assertTrue(c.getPosizionamentoScavo().getActualContent().isEmpty());

		//Clear pianta [0]
		c=client.cleanFileSet(c.getMongo_id(),Paths.piantaByIndex(0));
		assertTrue(c.getPianteFineScavo().get(0).getActualContent().isEmpty());
	}


	// UTILS

	public static long count(Iterator<?> iterator){
		AtomicLong l=new AtomicLong(0);
		iterator.forEachRemaining(el->{l.incrementAndGet();});
		return l.get();
	}


}
