package org.gcube.application.geoportal.clients.legacy;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import org.gcube.application.geoportal.client.legacy.ConcessioniManager;
import org.gcube.application.geoportal.clients.BasicVreTests;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.GeoServerContent;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.PersistedContent;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport.ValidationStatus;
import org.junit.Test;

public class ConcessioniTests extends BasicVreTests {

	static boolean STOP_ON_FAIL=true;
	


	@Test
	public void readAll() throws Exception {
		ConcessioniManager manager=new ConcessioniManager();		
		ArrayList<Concessione> found=manager.getList();
		System.out.println("Found "+found.size()+" elements.");
		final AtomicLong byId=new AtomicLong(0);
		
		final ArrayList<Long> validIDs=new ArrayList<>();
		
		final ArrayList<Long> imgs=new ArrayList<Long>();
		final AtomicLong error=new AtomicLong(0);
		
		final ArrayList<Long> piante=new ArrayList<Long>();
		final ArrayList<Long> pos=new ArrayList<Long>();
		final ArrayList<Long> wmsLink=new ArrayList<Long>();
		
		found.forEach((Concessione c )->{
			try {
				manager.getById(c.getId()+"");
				byId.incrementAndGet();
			if(c.validate().getStatus().equals(ValidationStatus.PASSED))
					validIDs.add(c.getId());
			if(c.getImmaginiRappresentative()!=null&&c.getImmaginiRappresentative().size()>0)
				imgs.add(c.getId());
			if(c.getPianteFineScavo()!=null)
				c.getPianteFineScavo().forEach((LayerConcessione l)->{
					if(l.getActualContent()!=null)
						l.getActualContent().forEach((PersistedContent p)->{
							if(p instanceof GeoServerContent) piante.add(c.getId());
						});
					if(l.getWmsLink()!=null) wmsLink.add(c.getId());
				});
			
			if(c.getPosizionamentoScavo()!=null) {
				LayerConcessione l=c.getPosizionamentoScavo();
				l.getActualContent().forEach((PersistedContent p)->{
							if(p instanceof GeoServerContent) pos.add(c.getId());
						});
				if(l.getWmsLink()!=null) wmsLink.add(c.getId());
			}
			}catch(Throwable t) {				
				error.incrementAndGet();
				if(STOP_ON_FAIL) {
					throw new RuntimeException(t);
				}else t.printStackTrace(System.err);
			}
		});
		System.out.println("Valid count "+validIDs.size()+ "Load BY ID : "+byId.get()+" Error : "+error.get()+" OUT OF "+found.size());
		System.out.println("Valid IDS "+validIDs);
		System.out.println("With imgs : "+imgs);
		System.out.println("With piante : "+piante);
		System.out.println("With pos : "+pos);
		System.out.println("With wmsLink : "+wmsLink);
	}
	
	
	
	
	

}
