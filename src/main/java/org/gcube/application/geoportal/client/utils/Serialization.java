package org.gcube.application.geoportal.client.utils;

import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class Serialization {

public static final DateTimeFormatter FULL_FORMATTER=DateTimeFormatter.ofPattern("uuuuMMdd_HH-mm-ss");
	
	public static ObjectMapper mapper;
		
		static {
			mapper=new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			mapper.registerModule(new JavaTimeModule());			
		}
		
		public static <T> T read(String jsonString,Class<T> clazz) throws JsonProcessingException, IOException {
			return mapper.readerFor(clazz).readValue(jsonString);
		}
		
		
		public static String write(Object obj) throws JsonProcessingException, IOException {
			return mapper.writeValueAsString(obj);
		}

		public static <T> Iterator<T> readCollection(String jsonString, Class<T> clazz) throws IOException {
			return mapper.readerFor(clazz).readValues(jsonString);
		}

	public static <T> Iterator<T> readCollection(InputStream is, Class<T> clazz) throws IOException {
		return mapper.readerFor(clazz).readValues(is);
	}
}
