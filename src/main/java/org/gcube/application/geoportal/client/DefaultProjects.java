package org.gcube.application.geoportal.client;

import java.rmi.RemoteException;
import java.util.Iterator;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.gcube.application.geoportal.common.model.project.Project;
import org.gcube.application.geoportal.common.rest.ProjectsI;
import org.gcube.common.clients.Call;
import org.gcube.common.clients.delegates.ProxyDelegate;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DefaultProjects implements ProjectsI{

	@NonNull
	private final ProxyDelegate<WebTarget> delegate;
	
	@Override
	public void deleteById(String profileId, String projectId) throws RemoteException {
		deleteById(profileId,projectId,false);
	}
		@Override
	public void deleteById(String profileId, String projectId,Boolean force) throws RemoteException {
//		Call<WebTarget,String> call= new Call<WebTarget, String>() {
//			
//			@Override
//			public String call(WebTarget endpoint) throws Exception {
//				endpoint.path(profileId).path(projectId).request(MediaType.APPLICATION_JSON).delete();
//			}
//		};
//		try{
//			delegate.make(call);
//		}catch(Exception e) {
////			throw new RemoteException(e);
//		}
	}	
	
	@Override
	public Iterator<Project> getAll() throws RemoteException {
		Call<WebTarget,Iterator<Project>> call=new Call<WebTarget,Iterator<Project>>(){
			@Override
			public Iterator<Project> call(WebTarget endpoint) throws Exception {
				throw new Exception("Client method not ready");
			}	
		};
		try{
			return delegate.make(call);
		}catch(Exception e) {
			throw new RemoteException(e.getMessage());
		}
	}
	
	@Override
	public Iterator<Project> getByFilter(String filter) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Iterator<Project> getByFilter(String filter, String profileId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Project getById(String profileId, String id) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Iterator<Project> getByProfile(String profileId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Project registrNew(String profileId, String jsonDocument) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Project update(String profileId, String projectId, String jsonDocument) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
}
