package org.gcube.application.geoportal.client.utils;

import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class FileSets {

    public static class RequestBuilder {
        AddSectionToConcessioneRequest theRequest=new AddSectionToConcessioneRequest();

        public RequestBuilder add(TempFile... f){
            if(theRequest.getStreams()==null)
                theRequest.setStreams(new ArrayList<TempFile>());
            for(TempFile temp: f )
                theRequest.getStreams().add(temp);
            return this;
        }

        public RequestBuilder add(TempFile f){
            if(theRequest.getStreams()==null)
                theRequest.setStreams(new ArrayList<TempFile>());
            theRequest.getStreams().add(f);
            return this;
        }

        public RequestBuilder setPath(String path){
            theRequest.setDestinationPath(path);
            return this;
        }





        public AddSectionToConcessioneRequest getTheRequest(){return theRequest;}
    }


    public static RequestBuilder build(String path) {
        return new RequestBuilder().setPath(path);
    }

    public static RequestBuilder build(String path, TempFile...files) {
        return new RequestBuilder().setPath(path).add(files);
    }

    public static TempFile asTemp(StorageUtils storage,InputStreamDescriptor descriptor) throws RemoteBackendException, FileNotFoundException {
        return storage.putOntoStorage(descriptor.getStream(), descriptor.getFilename());
    }

    public static TempFile[] asTemp(StorageUtils storage,InputStreamDescriptor... descriptors) throws RemoteBackendException, FileNotFoundException {
        ArrayList<TempFile> toReturn=new ArrayList<TempFile>();
        for(InputStreamDescriptor desc:descriptors)
            toReturn.add(storage.putOntoStorage(desc.getStream(), desc.getFilename()));
        return toReturn.toArray(new TempFile[toReturn.size()]);
    }

    public static AddSectionToConcessioneRequest prepareRequestFromFolder(StorageUtils storage, String path, File directory) throws FileNotFoundException {

        File[] children =directory.listFiles();
        System.out.println("Found "+children+ " files to push");
        InputStreamDescriptor[] iss=new InputStreamDescriptor[children.length];
        return prepareRequest(storage,path,children);

    }

    public static AddSectionToConcessioneRequest prepareRequest(StorageUtils storage, String path, File... toUpload) throws FileNotFoundException {

        FileSets.RequestBuilder builder = FileSets.build(path);
        for (File f : toUpload) {
            if(!f.isDirectory())
            builder.add(FileSets.asTemp(storage, new InputStreamDescriptor(new FileInputStream(f), f.getName())));
        }
        return builder.getTheRequest();
    }
}
