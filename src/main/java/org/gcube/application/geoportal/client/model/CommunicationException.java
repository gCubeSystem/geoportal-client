package org.gcube.application.geoportal.client.model;

import lombok.Data;

@Data
public class CommunicationException extends Exception {

	private String remoteMessage;
	private Integer responseHTTPCode;
	
	public CommunicationException() {
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
