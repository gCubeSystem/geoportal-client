package org.gcube.application.geoportal.client.legacy;

import static  org.gcube.application.geoportal.client.GeoportalAbstractPlugin.concessioni;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.legacy.report.PublicationReport;
import org.gcube.application.geoportal.common.rest.ConcessioniI;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConcessioniManager{

	private static ObjectMapper mapper = new ObjectMapper();

	private static ObjectReader concessioniReader=null;
	private static ObjectReader collectionReader=null;
	
	static {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		//		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		
		
		concessioniReader=mapper.readerFor(Concessione.class);
		collectionReader=mapper.readerFor(Collection.class);
	}

	private ConcessioniI service;

	private static enum ImplementationType{
		
	}
	
	public ConcessioniManager() {
		service=concessioni().build();
	}
	
	

	
	
	public Concessione getById(String id) throws Exception {
		log.info("Reading by ID "+id);
		try {
			String result=service.readById(id);
			log.debug("Reading json object : "+result);
			return concessioniReader.readValue(result);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static String toJson(Object obj) throws JsonProcessingException {
		return mapper.writeValueAsString(obj);
	}
	
	
	public ArrayList<Concessione> getList() throws Exception{
		log.info("Getting list");
		try {
			String result=service.getAll();
			log.debug("Reading json object : "+result);
			
			
			Collection<String> coll=collectionReader.readValue(result);
//			JsonArray array=Json.createReader(new ByteArrayInputStream(result.getBytes(StandardCharsets.UTF_8))).readArray();
			ArrayList<Concessione> toReturn=new ArrayList<Concessione>();
			
//			MappingIterator<Concessione> it=concessioniReader.readValues(result);
//			while(it.hasNext())
//				toReturn.add(it.next());
			
			for(String s: coll) {
				toReturn.add(concessioniReader.readValue(s));
			}
			
			
//			for(int i=0;i<array.size();i++)
//			for()
//				toReturn.add(concessioniReader.readValue(array.get(i).toString()));
			return toReturn;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	// Register session
	private Concessione lastRegistered;
	
	public Concessione registerNew(Concessione c) throws Exception {
		try {
			String response=service.create(mapper.writeValueAsString(c));
			lastRegistered=concessioniReader.readValue(response);
			return lastRegistered;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	
	public void setRelazioneScavo(RelazioneScavo rel, InputStreamDescriptor theFile) {
//		String response=service.
	}
	
	public void addImmagineRappresentativa(UploadedImage img, InputStreamDescriptor theFile) {
		
	}
	
	public void setPosizionamento(LayerConcessione layer, InputStreamDescriptor...inputStreams ) {
		
	}
	
	public void addPiantaScavo(LayerConcessione layer, InputStreamDescriptor...inputStreams ) {
		
	}
	
	public PublicationReport commitRegistered() {
		return null;
	}
}
