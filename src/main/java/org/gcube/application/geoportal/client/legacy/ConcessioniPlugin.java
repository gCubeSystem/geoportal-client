package org.gcube.application.geoportal.client.legacy;

import javax.ws.rs.client.WebTarget;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMResult;
import javax.xml.ws.EndpointReference;

import org.gcube.application.geoportal.client.GeoportalAbstractPlugin;
import org.gcube.application.geoportal.common.rest.ConcessioniI;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.common.calls.jaxrs.GcubeService;
import org.gcube.common.calls.jaxrs.TargetFactory;
import org.gcube.common.clients.config.ProxyConfig;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.w3c.dom.Node;

public class ConcessioniPlugin extends GeoportalAbstractPlugin<WebTarget, ConcessioniI>{

	

	@Override
	public Exception convert(Exception fault, ProxyConfig<?, ?> config) {
		return fault;
	}
	
	@Override
	public ConcessioniI newProxy(ProxyDelegate<WebTarget> delegate) {
		return new DefaultConcessioni(delegate);
	}
	
	
	@Override
	public WebTarget resolve(EndpointReference address, ProxyConfig<?, ?> config) throws Exception {
		DOMResult result = new DOMResult();
		address.writeTo(result);
		Node node =result.getNode();
		Node child=node.getFirstChild();
		String addressString = child.getTextContent();
		GcubeService service = GcubeService.service().
				withName(new QName(InterfaceConstants.NAMESPACE,InterfaceConstants.Methods.CONCESSIONI)).
				andPath(InterfaceConstants.Methods.CONCESSIONI);		
		return TargetFactory.stubFor(service).at(addressString);
	}
}
