package org.gcube.application.geoportal.client;

import javax.ws.rs.client.WebTarget;

import org.gcube.application.geoportal.client.legacy.ConcessioniManagerI;
import org.gcube.application.geoportal.client.legacy.ConcessioniPlugin;
import org.gcube.application.geoportal.client.legacy.MongoConcessioniPlugin;
import org.gcube.application.geoportal.client.legacy.StatefulMongoConcessioni;
import org.gcube.application.geoportal.client.legacy.StatefulMongoConcessioniPlugin;
import org.gcube.application.geoportal.common.rest.ConcessioniI;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.common.clients.Plugin;
import org.gcube.common.clients.ProxyBuilder;
import org.gcube.common.clients.ProxyBuilderImpl;
import org.gcube.common.clients.config.ProxyConfig;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class GeoportalAbstractPlugin <S, P> implements Plugin<S, P>{


	
	private static ConcessioniPlugin concessioni_plugin=new ConcessioniPlugin();
	
	private static MongoConcessioniPlugin mongo_concessioni_plugin=new MongoConcessioniPlugin();
	
	private static StatefulMongoConcessioniPlugin stateful_mongo_concessioni_plugin=new StatefulMongoConcessioniPlugin();
	
	public static ProxyBuilder<ConcessioniI> concessioni() {
	    return new ProxyBuilderImpl<WebTarget,ConcessioniI>(concessioni_plugin);
	}
	
	public static ProxyBuilder<MongoConcessioni> mongoConcessioni(){
		return new ProxyBuilderImpl<WebTarget, MongoConcessioni>(mongo_concessioni_plugin);
	}
	
	public static ProxyBuilder<ConcessioniManagerI> statefulMongoConcessioni(){
		return new ProxyBuilderImpl<WebTarget, ConcessioniManagerI>(stateful_mongo_concessioni_plugin);
	}
	
	
	@Override
	public Exception convert(Exception fault, ProxyConfig<?, ?> config) {
		return fault;
	}

	
	@Override
	public String name() {
		return InterfaceConstants.APPLICATION_BASE_PATH+InterfaceConstants.APPLICATION_PATH;
	}

	@Override
	public String namespace() {
		return InterfaceConstants.NAMESPACE;
	}

	@Override
	public String serviceClass() {
		return InterfaceConstants.SERVICE_CLASS;
	}

	@Override
	public String serviceName() {
		return InterfaceConstants.SERVICE_NAME;
	}

	
	
}
