package org.gcube.application.geoportal.client;

import javax.ws.rs.client.WebTarget;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMResult;
import javax.xml.ws.EndpointReference;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.ProjectsI;
import org.gcube.common.calls.jaxrs.GcubeService;
import org.gcube.common.calls.jaxrs.TargetFactory;
import org.gcube.common.clients.config.ProxyConfig;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.w3c.dom.Node;

public class ProjectsPlugin extends GeoportalAbstractPlugin<WebTarget, ProjectsI>{
	
	
	@Override
	public Exception convert(Exception fault, ProxyConfig<?, ?> config) {
		return fault;
	}
	
	@Override
	public ProjectsI newProxy(ProxyDelegate<WebTarget> delegate) {
		return new DefaultProjects(delegate);
	}
	
	@Override
	public WebTarget resolve(EndpointReference address, ProxyConfig<?, ?> config) throws Exception {
		DOMResult result = new DOMResult();
		address.writeTo(result);
		Node node =result.getNode();
		Node child=node.getFirstChild();
		String addressString = child.getTextContent();
		GcubeService service = GcubeService.service().
				withName(new QName(InterfaceConstants.NAMESPACE,InterfaceConstants.Methods.PROJECTS)).
				andPath(InterfaceConstants.Methods.PROJECTS);		
		return TargetFactory.stubFor(service).at(addressString);
	}
}
