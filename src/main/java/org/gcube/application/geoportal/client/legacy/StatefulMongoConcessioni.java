package org.gcube.application.geoportal.client.legacy;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.ws.rs.client.WebTarget;

import org.gcube.application.geoportal.client.DefaultMongoConcessioni;
import org.gcube.application.geoportal.client.utils.FileSets;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.Concessione.Paths;
import org.gcube.application.geoportal.common.model.legacy.InputStreamDescriptor;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.common.clients.delegates.ProxyDelegate;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;


public class StatefulMongoConcessioni extends DefaultMongoConcessioni implements ConcessioniManagerI{

	
	
	public StatefulMongoConcessioni(ProxyDelegate<WebTarget> delegate) {
		super(delegate);
	}

	private Concessione currentC=null;
	private StorageUtils storage=new StorageUtils();


	// Override methods to handle state
	@Override
	public Concessione createNew(Concessione c) throws Exception {
		currentC=super.createNew(c);
		return currentC;
	}

	@Override
	public Concessione getById(String id) throws Exception {
		currentC= super.getById(id);
		return currentC;
	}

	@Override
	public Concessione publish() throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		currentC=super.publish(currentC.getMongo_id());
		return currentC;
	}

	
	@Override
	public void delete() throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		super.deleteById(currentC.getMongo_id());
		currentC=null;
	}

	@Override
	public Concessione addImmagineRappresentativa(UploadedImage toAdd, TempFile f) throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		if(currentC.getImmaginiRappresentative()==null) currentC.setImmaginiRappresentative(new ArrayList<UploadedImage>());
		currentC.getImmaginiRappresentative().add(toAdd);
		currentC=replace(currentC);
		
		
		currentC=super.registerFileSet(currentC.getMongo_id(),
				FileSets.build(Paths.imgByIndex(currentC.getImmaginiRappresentative().size()-1),f).getTheRequest());
		return currentC;
	}

	@Override
	public Concessione addPiantaFineScavo(LayerConcessione toAdd, TempFile... files) throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		if(currentC.getPianteFineScavo()==null) currentC.setPianteFineScavo(new ArrayList<>());
		currentC.getPianteFineScavo().add(toAdd);
		currentC=replace(currentC);
		
		
		currentC=super.registerFileSet(currentC.getMongo_id(),
				FileSets.build(Paths.piantaByIndex(currentC.getPianteFineScavo().size()-1),files).getTheRequest());
		return currentC;
	}

	@Override
	public Concessione setPosizionamento(LayerConcessione toSet, TempFile... files) throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		currentC.setPosizionamentoScavo(toSet);
		currentC=replace(currentC);
		
		
		currentC=super.registerFileSet(currentC.getMongo_id(),
				FileSets.build(Paths.POSIZIONAMENTO,files).getTheRequest());
		return currentC;
	}

	@Override
	public Concessione setRelazioneScavo(RelazioneScavo toSet, TempFile... files) throws Exception {
		if(currentC==null) throw new Exception("Invalid operation : current Concessione is null.");
		currentC.setRelazioneScavo(toSet);
		currentC=replace(currentC);
		
		
		currentC=super.registerFileSet(currentC.getMongo_id(),
				FileSets.build(Paths.RELAZIONE,files).getTheRequest());
		return currentC;
	}



	@Override
	public Concessione addImmagineRappresentativa(UploadedImage toAdd, InputStreamDescriptor f) throws Exception {
		return addImmagineRappresentativa(toAdd,FileSets.asTemp(storage,f));
	}

	@Override
	public Concessione addPiantaFineScavo(LayerConcessione toAdd, InputStreamDescriptor... files) throws Exception {
		return addPiantaFineScavo(toAdd, FileSets.asTemp(storage,files));
	}

	@Override
	public Concessione setPosizionamento(LayerConcessione toSet, InputStreamDescriptor... files) throws Exception {
		return setPosizionamento(toSet,FileSets.asTemp(storage,files));
	}

	@Override
	public Concessione setRelazioneScavo(RelazioneScavo toSet, InputStreamDescriptor f) throws Exception {
		return setRelazioneScavo(toSet,FileSets.asTemp(storage,f));
	}

}
