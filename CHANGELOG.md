This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.geoportal-client

## [v1.0.4-SNAPSHOT] - 2020-11-11
Serialization utils
Queries & Filters support
Test Reports
UseCases
Fixes #21897

## [v1.0.3] - 2020-11-11
Stateful Concessioni Manager client over mongo 

## [v1.0.2] - 2020-11-11
Fixed dulicate dependency declaration

## [v1.0.1] - 2020-11-11
Excluded common-calls 1.2.0


## [v1.0.0] - 2020-11-11

First release